import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  Typography,
} from "@mui/material";
import React from "react";
import { useAppSelector } from "Store";
import { selectProjectData } from "Store/slices/portfolioDataSlice";

const Projects = () => {
  const projects = useAppSelector(selectProjectData);
  return (
    <Grid container spacing={3}>
      {projects.map((project, index) => (
        <Grid item xs={12} key={project.id} data-aos="fade-up">
          <Grid
            container
            component={Card}
            elevation={4}
            sx={{
              height: { md: 300 },
              flexDirection: index % 2 === 0 ? "row" : "row-reverse",
            }}
          >
            <Grid
              item
              xs={12}
              md={7}
              component={CardContent}
              sx={{ display: "flex", flexDirection: "column", height: 1 }}
            >
              <Typography component="div" variant="h5" fontWeight={700}>
                {project.name}
              </Typography>
              <Typography
                variant="subtitle2"
                color="text.secondary"
                component="div"
                fontWeight={600}
                sx={{
                  flexGrow: 1,
                }}
              >
                {project.description.length > 530
                  ? project.description.slice(0, 530) + "..."
                  : project.description}
              </Typography>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  my: "10px",
                  gap: 1.5,
                }}
              >
                {project.links?.map((link) => {
                  return (
                    <Button
                      color="secondary"
                      key={link.link}
                      sx={{
                        backgroundColor: "rgba(0, 0, 0, 0.2)",
                        borderRadius: "0.25rem",
                        padding: "0.2rem 0.5rem",
                        fontWeight: "600",
                        "&:hover": {
                          backgroundColor: "rgba(0, 0, 0, 0.3)",
                        },
                      }}
                      onClick={() => {
                        window.open(link.link, "_blank");
                      }}
                    >
                      {link.name}
                    </Button>
                  );
                })}
              </Box>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  flexWrap: "wrap",
                  gap: 1,
                }}
              >
                {project.tags?.map((tag, idx) => (
                  <Typography
                    key={idx}
                    variant="subtitle2"
                    component="div"
                    sx={{
                      backgroundColor: "rgba(0, 0, 0, 0.2)",
                      borderRadius: "0.25rem",
                      padding: "0.2rem 0.5rem",
                    }}
                  >
                    {tag}
                  </Typography>
                ))}
              </Box>
            </Grid>
            <Grid
              item
              xs={12}
              md={5}
              component="img"
              sx={{
                width: 1,
                height: 1,
                objectFit: "cover",
                objectPosition: "top",
                borderRadius: "5px",
              }}
              src={project.cover}
              alt={project.name}
            />
          </Grid>
        </Grid>
      ))}
    </Grid>
  );
};

export default Projects;
