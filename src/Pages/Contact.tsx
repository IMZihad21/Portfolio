import React, { useState } from "react";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  TextField,
} from "@mui/material";
import { useForm } from "react-hook-form";
import { send } from "@emailjs/browser";

const Contact = () => {
  const [response, setResponse] = useState<string | null>(null);
  const [submitting, setSubmitting] = useState<boolean>(false);
  const {
    handleSubmit,
    register,
    reset,
    formState: { errors },
  } = useForm();

  const submitMessage = async (data: { [x: string]: string }) => {
    setSubmitting(true);
    const res = await send(
      "service_mail_google",
      "mofajjal_mailer",
      {
        from_name: data.fullName,
        message: data.message,
        reply_to: data.email,
      },
      "SK_4jGf0kf9N9DDUQ"
    );
    if (res.status === 200) {
      setResponse("Thank you for your message! I will get back to you soon :)");
      reset();
    } else {
      setResponse("Seems like something went wrong. Please try again :(");
    }
    setSubmitting(false);
  };

  return (
    <React.Fragment>
      <Box
        component="form"
        data-aos="fade-up"
        onSubmit={handleSubmit(submitMessage)}
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          gap: "20px",
          marginTop: "20px",
        }}
      >
        <TextField
          label="Full Name"
          color="secondary"
          variant="outlined"
          error={!!errors.fullName}
          helperText={errors.fullName?.message}
          sx={{
            width: {
              xs: "90%",
              lg: "600px",
            },
          }}
          {...register("fullName", {
            required: "Name is required",
          })}
        />
        <TextField
          label="Email"
          color="secondary"
          variant="outlined"
          error={!!errors.email}
          helperText={errors.email?.message}
          sx={{
            width: {
              xs: "90%",
              lg: "600px",
            },
          }}
          {...register("email", {
            required: "Email is required",
            pattern: {
              value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
              message: "Invalid email address",
            },
          })}
        />
        <TextField
          label="Message"
          color="secondary"
          variant="outlined"
          error={!!errors.message}
          helperText={errors.message?.message}
          multiline
          rows={10}
          sx={{
            width: {
              xs: "90%",
              lg: "600px",
            },
          }}
          {...register("message", {
            required: "Message is required",
          })}
        />
        <Button
          color="secondary"
          variant="contained"
          type="submit"
          disabled={submitting}
          sx={{
            width: {
              xs: "90%",
              lg: "600px",
            },
            height: "40px",
            fontSize: "16px",
            fontWeight: "bold",
          }}
        >
          Submit
        </Button>
      </Box>
      <Dialog open={!!response} onClose={() => setResponse(null)}>
        <DialogContent
          sx={{
            minWidth: { lg: "300px" },
            minHeight: "100px",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <DialogContentText
            color="secondary"
            sx={{
              fontSize: { lg: "20px" },
              fontWeight: "600",
            }}
          >
            {response}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            color="secondary"
            variant="contained"
            onClick={() => setResponse(null)}
            autoFocus
            sx={{
              height: "40px",
              fontSize: { lg: "16px" },
              fontWeight: "bold",
              px: "50px",
            }}
          >
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
};

export default Contact;
