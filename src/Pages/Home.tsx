import React from "react";
// import { Box, Button, Typography } from "@mui/material";
import { Box, Typography } from "@mui/material";
import { useAppSelector } from "Store";
import { selectGithubProfile } from "Store/slices/githubProfileSlice";
import Markdown from "markdown-to-jsx";
import { useTypewriter, Cursor } from "react-simple-typewriter";

const Home = () => {
  // const { githubProfile, resumeId } = useAppSelector(selectGithubProfile);
  const { githubProfile } = useAppSelector(selectGithubProfile);
  const { text } = useTypewriter({
    words: [
      "Hello",
      "Konnichiwa",
      "Asalaam alaikum",
      "Bonjour",
      "Hola",
      "Zdravstvuyte",
      "Nǐn hǎo",
      "Salve",
      "Guten Tag",
      "Olá",
      "Anyoung haseyo",
      "Goddag",
      "Shikamoo",
      "Goedendag",
      "Yassas",
      "Dzień dobry",
      "Selamat siang",
      "Merhaba",
      "Shalom",
      "God dag",
    ],
    loop: 0, // Infinit
  });

  return (
    <Box sx={{ my: 2 }}>
      <Box
        sx={{
          mb: 2,
          display: { md: "flex" },
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Typography variant="h5" fontWeight={700}>
          {text}
          <Cursor cursorStyle="!" />
        </Typography>
        {/* 
          Disable download resume button for now
        */}
        {/* <Button
          color="secondary"
          variant="outlined"
          sx={{
            height: "35px",
            mt: 2,
          }}
          onClick={() => {
            window.location.href = `https://drive.google.com/uc?id=${resumeId}&export=download`;
          }}
        >
          Download Resume
        </Button> */}
      </Box>
      <Markdown data-aos="fade-up" children={githubProfile} />
    </Box>
  );
};

export default Home;
