import React from "react";
import { Box, Button, Grid, Paper, Typography } from "@mui/material";
import { useAppSelector } from "Store";
import { selectBlogData } from "Store/slices/portfolioDataSlice";

const Blogs = () => {
  const blogs = useAppSelector(selectBlogData);
  return (
    <Grid
      container
      sx={{
        mb: 2,
      }}
      spacing={2}
    >
      {blogs.map((blog) => (
        <Grid item xs={12} md={6} key={blog.blog_id} data-aos="fade-up">
          <Paper
            sx={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              height: 1,
              padding: 2,
              mb: 3,
            }}
          >
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "start",
              }}
            >
              <Typography variant="h5" sx={{ mb: 2, fontWeight: "600" }}>
                {blog.blog_title}
              </Typography>
              <Button
                variant="outlined"
                color="secondary"
                sx={{
                  height: "30px",
                }}
              >
                Visit Blog
              </Button>
            </Box>
            <Typography variant="body1" sx={{ mb: 2, flexGrow: 1 }}>
              {blog.blog_content.length > 300
                ? blog.blog_content.slice(0, 300) + "..."
                : blog.blog_content}
            </Typography>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Typography variant="body2" sx={{ fontWeight: "bold" }}>
                {blog.blog_author}
              </Typography>
              <Typography variant="body2">{blog.blog_date}</Typography>
            </Box>
          </Paper>
        </Grid>
      ))}
    </Grid>
  );
};

export default Blogs;
