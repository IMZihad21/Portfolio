import { Box, Typography } from "@mui/material";
import React from "react";
import { useAppSelector } from "Store";
import { selectAboutMeData } from "Store/slices/portfolioDataSlice";

const About = () => {
  const aboutMe = useAppSelector(selectAboutMeData);
  return (
    <Box
      data-aos="fade-up"
      sx={{
        my: 6,
      }}
    >
      {aboutMe.map((paragraph) => (
        <Typography
          key={paragraph}
          variant="body1"
          sx={{
            my: 3,
          }}
        >
          {paragraph}
        </Typography>
      ))}
    </Box>
  );
};

export default About;
