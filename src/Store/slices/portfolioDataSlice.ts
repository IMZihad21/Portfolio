import { AnyAction, createSlice, ThunkAction } from "@reduxjs/toolkit";
import PortfolioDataInterface from "Interfaces/PortfolioDataInterface";
import type { RootState } from "Store";
import portfolioData from "../jsonData/portfolioData.json";

// Define a type for the slice state
interface PortfolioDataSliceInterface extends PortfolioDataInterface {
  loading: boolean;
}

// Define the initial state using that type
const initialState: PortfolioDataSliceInterface = {
  loading: false,
  projects: [],
  aboutMe: [],
  blogs: [],
};

const portfolioDataSlice = createSlice({
  name: "portfolioDataSlice",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    // // `addProject` is a reducer
    // addProject: (state, { payload }) => {
    //   state.projects = payload;
    // },
    // // `addAboutMe` is a reducer
    // addAboutMe: (state, { payload }) => {
    //   state.aboutMe = payload;
    // },
    // // `addBlogs` is a reducer
    // addBlogs: (state, { payload }) => {
    //   state.blogs = payload;
    // },
    addPortfolioData: (state, { payload }) => {
      state.projects = payload.projects;
      state.aboutMe = payload.aboutMe;
      state.blogs = payload.blogs;
    },
    // `setLoading` is a reducer
    setLoading: (state, { payload }) => {
      state.loading = payload;
    },
  },
});

const { addPortfolioData, setLoading } = portfolioDataSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const selectProjectData = (state: RootState) =>
  state.portfolioData.projects;
export const selectAboutMeData = (state: RootState) =>
  state.portfolioData.aboutMe;
export const selectBlogData = (state: RootState) => state.portfolioData.blogs;
export const selectPortfolioDataLoading = (state: RootState) =>
  state.portfolioData.loading;

// create a thunk action to fetch the data from the server
export const fetchPortfolioData =
  (): ThunkAction<void, RootState, unknown, AnyAction> => async (dispatch) => {
    // set the loading state to true
    dispatch(setLoading(true));
    let data: PortfolioDataInterface;
    try {
      const response = await fetch(
        "https://gist.githubusercontent.com/IMZihad21/87c7211efa951e1f10b4d4f5c89fc5d5/raw/portfolioData.json"
      );
      data = await response.json();
    } catch (error) {
      data = portfolioData;
      console.log(error);
    }
    dispatch(addPortfolioData(data));
    setTimeout(() => {
      dispatch(setLoading(false));
    }, 500);
  };

export default portfolioDataSlice.reducer;
