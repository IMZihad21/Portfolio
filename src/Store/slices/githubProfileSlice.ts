import { AnyAction, createSlice, ThunkAction } from "@reduxjs/toolkit";
import type { RootState } from "Store";

// Define the initial state interface
interface GithubProfileInterface {
  loading: boolean;
  githubProfile: string;
  resumeId: string;
}

// Define the initial state using that type
const initialState: GithubProfileInterface = {
  loading: false,
  githubProfile: "",
  resumeId: "1B4sFcodCyODdUGIxGfNUgUNXkakL3E1d",
};

const githubProfileSlice = createSlice({
  name: "githubProfileSlice",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    // `addGithubProfile` is a reducer
    addGithubProfile: (state, { payload }) => {
      state.githubProfile = payload;
    },
    // `setLoading` is a reducer
    setLoading: (state, { payload }) => {
      state.loading = payload;
    },
  },
});

const { addGithubProfile, setLoading } = githubProfileSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const selectGithubProfile = (state: RootState) => state.profileData;
export const selectGithubProfileLoading = (state: RootState) =>
  state.profileData.loading;

// create a thunk action to fetch the data from the server
export const fetchGithubProfile =
  (): ThunkAction<void, RootState, unknown, AnyAction> => async (dispatch) => {
    // set the loading state to true
    dispatch(setLoading(true));
    try {
      const response = await fetch(
        "https://raw.githubusercontent.com/IMZihad21/IMZihad21/main/README.md"
      );
      const data = await response.text();
      dispatch(addGithubProfile(data));
      setTimeout(() => {
        dispatch(setLoading(false));
      }, 500);
    } catch (error) {
      console.log(error);
      dispatch(setLoading(false));
    }
  };

export default githubProfileSlice.reducer;
