import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";
import themeModeSlice from "Store/slices/themeModeSlice";
import portfolioDataSlice from "Store/slices/portfolioDataSlice";
import githubProfileSlice from "Store/slices/githubProfileSlice";

const store = configureStore({
  reducer: {
    themeMode: themeModeSlice,
    portfolioData: portfolioDataSlice,
    profileData: githubProfileSlice,
  },
});

export default store;

// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
type AppDispatch = typeof store.dispatch;
// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;

// Use throughout your app instead of plain `useDispatch` and `useSelector`
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
