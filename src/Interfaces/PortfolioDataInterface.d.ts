interface PortfolioDataInterface {
  projects: Project[];
  aboutMe: string[];
  blogs: Blog[];
}

interface Project {
  id: number;
  name: string;
  cover: string;
  links: Link[];
  description: string;
  tags?: string[];
  screenshots?: string[];
}

interface Link {
  name: string;
  link: string;
}

interface Blog {
  blog_id: string;
  blog_title: string;
  blog_content: string;
  blog_author: string;
  blog_date: string;
}

export default PortfolioDataInterface;
