import React, { useEffect } from "react";
import { Navigate, Outlet, useRoutes } from "react-router-dom";
import { Box, Container } from "@mui/material";
import ThemeLayout from "Components/ThemeLayout";
import NavBar from "Components/NavBar";
import Footer from "Components/Footer";
import Home from "Pages/Home";
import { useAppDispatch, useAppSelector } from "Store";
import {
  fetchPortfolioData,
  selectPortfolioDataLoading,
} from "Store/slices/portfolioDataSlice";
import {
  fetchGithubProfile,
  selectGithubProfileLoading,
} from "Store/slices/githubProfileSlice";
import Loader from "Components/Loader";
import About from "Pages/About";
import Projects from "Pages/Projects";
import Blogs from "Pages/Blogs";
import Contact from "Pages/Contact";
import AOS from "aos";
import "aos/dist/aos.css";

export default function App() {
  const portfolioLoading = useAppSelector(selectPortfolioDataLoading);
  const profileLoading = useAppSelector(selectGithubProfileLoading);
  const dispatch = useAppDispatch();

  // Fetch portfolio data from the server
  useEffect(() => {
    dispatch(fetchGithubProfile());
    dispatch(fetchPortfolioData());
    AOS.init({
      startEvent: "DOMContentLoaded",
    });
  }, [dispatch]);

  // React-Router-Dom uses `useRoutes` to get the routes from the `routes` object.
  const routes = [
    {
      path: "/",
      element: portfolioLoading || profileLoading ? <Loader /> : <Outlet />,
      children: [
        {
          index: true,
          element: <Home />,
        },
        {
          path: "about",
          element: <About />,
        },
        {
          path: "projects",
          element: <Projects />,
        },
        {
          path: "blogs",
          element: <Blogs />,
        },
        {
          path: "contact",
          element: <Contact />,
        },
        {
          path: "*",
          element: <Navigate to="/" />,
        },
      ],
    },
  ];
  const allRoutes = useRoutes(routes);

  return (
    <ThemeLayout>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          minHeight: "100vh",
        }}
      >
        <NavBar />
        <Container
          component="main"
          maxWidth="md"
          sx={{
            flexGrow: 1,
          }}
        >
          {allRoutes}
        </Container>
        <Footer />
      </Box>
    </ThemeLayout>
  );
}
