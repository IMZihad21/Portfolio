import React from "react";
import { ThemeProvider, CssBaseline } from "@mui/material";
import { createTheme } from "@mui/material/styles";
import { selectThemeMode } from "Store/slices/themeModeSlice";
import { useAppSelector } from "Store";

interface ThemeLayoutProps {
  children: React.ReactNode;
}

const ThemeLayout: React.FC<ThemeLayoutProps> = ({ children }) => {
  const themeMode = useAppSelector(selectThemeMode);
  const theme = createTheme({
    palette: {
      mode: themeMode,
      primary: {
        main: themeMode === "light" ? "#fff" : "#000",
      },
      secondary: {
        main: themeMode === "light" ? "#000" : "#fff",
      },
      text: {
        primary: themeMode === "light" ? "#000" : "#fff",
      },
      background: {
        default: themeMode === "light" ? "#fff" : "#000",
        paper: themeMode === "light" ? "#fff" : "#000",
      },
      contrastThreshold: 3,
      tonalOffset: 0.2,
    },
    typography: {
      fontFamily: ["Source Sans Pro", "sans-serif"].join(","),
    },
    components: {
      MuiCssBaseline: {
        styleOverrides: {
          body: {
            "& a": {
              textDecoration: "none !important",
              color: "inherit",
            },
            "&::-webkit-scrollbar, & *::-webkit-scrollbar": {
              backgroundColor: themeMode === "light" ? "#fff" : "#000",
              width: "5px",
            },
            "&::-webkit-scrollbar-thumb, & *::-webkit-scrollbar-thumb": {
              borderRadius: 5,
              backgroundColor: themeMode === "light" ? "#000" : "#fff",
              minHeight: 24,
            },
            "&::-webkit-scrollbar-thumb:focus, & *::-webkit-scrollbar-thumb:focus":
              {
                backgroundColor: "#81ADC8",
              },
            "&::-webkit-scrollbar-thumb:active, & *::-webkit-scrollbar-thumb:active":
              {
                backgroundColor: themeMode === "light" ? "#000" : "#fff",
              },
            "&::-webkit-scrollbar-thumb:hover, & *::-webkit-scrollbar-thumb:hover":
              {
                backgroundColor: "#81ADC8",
              },
            "&::-webkit-scrollbar-corner, & *::-webkit-scrollbar-corner": {
              backgroundColor: themeMode === "light" ? "#000" : "#fff",
            },
          },
        },
      },
      MuiTabs: {
        styleOverrides: {
          root: {
            minHeight: "25px",
            "& .MuiTabs-indicator": {
              backgroundColor:
                themeMode === "light" ? "#000 !important" : "#fff !important",
            },
            "& .Mui-selected": {
              color:
                themeMode === "light" ? "#000 !important" : "#fff !important",
              fontWeight: "600",
            },
            "& .MuiButtonBase-root": {
              padding: "10px",
              minHeight: "unset",
              minWidth: "unset",
              fontSize: "13px",
            },
          },
        },
      },
    },
  });
  return (
    <ThemeProvider theme={theme}>
      {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
};

export default ThemeLayout;
