import React from "react";
import {
  Box,
  Fab,
  Tooltip,
  Typography,
  useScrollTrigger,
  Zoom,
} from "@mui/material";
import GitHubIcon from "@mui/icons-material/GitHub";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import TocIcon from "@mui/icons-material/Toc";
import EmailIcon from "@mui/icons-material/Email";
import NavigationIcon from "@mui/icons-material/Navigation";

const socialLinks = [
  {
    label: "Github",
    href: "https://github.com/IMZihad21",
    icon: <GitHubIcon />,
  },
  {
    label: "LinkedIn",
    href: "https://www.linkedin.com/in/imzihad21",
    icon: <LinkedInIcon />,
  },
  {
    label: "StackOverflow",
    href: "https://stackoverflow.com/users/16378127",
    icon: <TocIcon />,
  },
  {
    label: "Email",
    href: "mailto:imzihad@gmail.com",
    icon: <EmailIcon />,
  },
];

const Footer = () => {
  const trigger = useScrollTrigger();
  const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
    const anchor = (
      (event.target as HTMLDivElement).ownerDocument || document
    ).querySelector("#back-to-top-anchor");

    if (anchor) {
      anchor.scrollIntoView({
        behavior: "smooth",
        block: "center",
      });
    }
  };

  return (
    <Box
      component="footer"
      sx={{
        padding: "15px",
        display: "flex",
        flexDirection: { xs: "column", md: "row" },
        justifyContent: "space-between",
        alignItems: { md: "center" },
        gap: { xs: "10px", md: "0" },
        mx: { xs: 0, md: 25 },
      }}
    >
      <Typography
        variant="body2"
        sx={{
          fontSize: { xs: "11px", md: "14px" },
          fontWeight: "bold",
        }}
      >
        Mofajjal Rasul
      </Typography>

      <Box
        sx={{
          display: "flex",
          flexDirection: { xs: "column", md: "row" },
          gap: { xs: "3px", md: "10px" },
        }}
      >
        {socialLinks.map((link) => (
          <Box
            component="a"
            href={link.href}
            key={link.href}
            sx={{
              display: "flex",
              alignItems: "center",
              marginRight: "10px",
            }}
          >
            {link.icon}
            <Typography
              variant="body2"
              sx={{
                fontSize: { xs: "10px", md: "12px" },
                fontWeight: "bold",
                marginLeft: "5px",
              }}
            >
              {link.label}
            </Typography>
          </Box>
        ))}
      </Box>

      <Typography
        variant="subtitle1"
        sx={{
          fontSize: { xs: "10px", md: "12px" },
          fontWeight: "bold",
        }}
      >
        @IMZihad21
      </Typography>

      <Zoom in={trigger}>
        <Tooltip
          onClick={handleClick}
          role="presentation"
          sx={{ position: "fixed", bottom: { xs: 16, md: 35 }, right: 16 }}
          title="Scroll to Top"
          arrow
          placement="left"
        >
          <Fab size="small">
            <NavigationIcon />
          </Fab>
        </Tooltip>
      </Zoom>
    </Box>
  );
};

export default Footer;
