import React, { useState } from "react";
import {
  AppBar,
  Box,
  Button,
  IconButton,
  SwipeableDrawer,
  Tab,
  Tabs,
  Toolbar,
  Typography,
} from "@mui/material";
import useScrollTrigger from "@mui/material/useScrollTrigger";
import Slide from "@mui/material/Slide";
import { useAppDispatch, useAppSelector } from "Store";
import { selectThemeMode, switchThemeMode } from "Store/slices/themeModeSlice";
import { useLocation, NavLink } from "react-router-dom";
import MenuIcon from "@mui/icons-material/Menu";
import Brightness4Icon from "@mui/icons-material/Brightness4";
import Brightness7Icon from "@mui/icons-material/Brightness7";

const navigationLinks = [
  {
    label: "Home",
    to: "/",
  },
  {
    label: "About",
    to: "/about",
  },
  {
    label: "Projects",
    to: "/projects",
  },
  {
    label: "Blogs",
    to: "/blogs",
  },
  {
    label: "Contact",
    to: "/contact",
  },
];

const NavBar = () => {
  const trigger = useScrollTrigger();
  const dispatch = useAppDispatch();
  const { pathname } = useLocation();
  const themeMode = useAppSelector(selectThemeMode);
  const [drawerOpen, setDrawerOpen] = useState(false);

  const toggleDrawer =
    (open: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event &&
        event.type === "keydown" &&
        ((event as React.KeyboardEvent).key === "Tab" ||
          (event as React.KeyboardEvent).key === "Shift")
      ) {
        return;
      }

      setDrawerOpen(open);
    };

  return (
    <React.Fragment>
      <Slide appear={false} direction="down" in={!trigger}>
        <AppBar
          component="nav"
          sx={{
            boxShadow: "none",
            backgroundImage: "none",
          }}
        >
          <Toolbar
            sx={{
              justifyContent: "space-between",
              mx: { xs: 0, md: 25 },
            }}
          >
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              onClick={() => setDrawerOpen(true)}
              sx={{
                display: { xs: "block", md: "none" },
                mr: 2,
              }}
            >
              <MenuIcon />
            </IconButton>
            <Typography component={NavLink} to="/" variant="h6">
              ZèD.
            </Typography>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
              }}
            >
              <Tabs
                value={pathname?.split("/").slice(0, 2).join("/")}
                sx={{
                  display: { xs: "none", md: "inline-flex" },
                }}
              >
                {navigationLinks.map((link) => (
                  <Tab
                    key={link.to}
                    component={NavLink}
                    to={link.to}
                    label={link.label}
                    value={link.to}
                    disableRipple
                  />
                ))}
              </Tabs>
              <Button
                disableRipple
                variant="text"
                onClick={() => dispatch(switchThemeMode())}
                sx={{
                  color: themeMode === "light" ? "#000" : "#fff",
                  padding: "10px",
                  paddingTop: "15px",
                  minHeight: "unset",
                  minWidth: "unset",
                }}
              >
                <Typography>
                  {themeMode === "dark" ? (
                    <Brightness7Icon />
                  ) : (
                    <Brightness4Icon />
                  )}
                </Typography>
              </Button>
            </Box>
          </Toolbar>
        </AppBar>
      </Slide>
      <Toolbar id="back-to-top-anchor" />
      <SwipeableDrawer
        anchor={"left"}
        open={drawerOpen}
        onClose={toggleDrawer(false)}
        onOpen={toggleDrawer(true)}
        sx={{
          display: { xs: "block", md: "none" },
        }}
      >
        <Typography
          component={NavLink}
          to="/"
          variant="h6"
          onClick={toggleDrawer(false)}
          onKeyDown={toggleDrawer(false)}
          sx={{
            textAlign: "center",
            fontSize: "20px",
            my: 2,
          }}
        >
          ZèD.
        </Typography>
        <Tabs
          value={pathname?.split("/").slice(0, 2).join("/")}
          role="presentation"
          orientation="vertical"
          onClick={toggleDrawer(false)}
          onKeyDown={toggleDrawer(false)}
          sx={{
            width: "auto",
            px: "20px",
          }}
        >
          {navigationLinks.map((link) => (
            <Tab
              key={link.to}
              component={NavLink}
              to={link.to}
              label={link.label}
              value={link.to}
              disableRipple
            />
          ))}
        </Tabs>
      </SwipeableDrawer>
    </React.Fragment>
  );
};

export default NavBar;
