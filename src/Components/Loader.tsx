import React from "react";
import { Box } from "@mui/material";
import { InfinitySpin } from "react-loader-spinner";
import { useAppSelector } from "Store";
import { selectThemeMode } from "Store/slices/themeModeSlice";

const Loader = () => {
  const themeMode = useAppSelector(selectThemeMode);
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "80vh",
      }}
    >
      <InfinitySpin
        width="200"
        color={themeMode === "light" ? "#000" : "#fff"}
      />
    </Box>
  );
};

export default Loader;
